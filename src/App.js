
import StepperForm from './stepper_form/stepper_form';
import StepperFormVertical from './stepper_form_vertical/stepper_form_vertical';

function App() {
  return (
    <div className="App">
      <StepperFormVertical/>
    </div>
  );
}

export default App;
