

import React, { useState ,useContext } from 'react';
import './stepper_form_vertical.css';

const StepperFormVertical = (props) => {

    const steps = [
        {
            label:"Information about the requesting entity",
            blocs:[
                [
                    {field:'company',type:'checkbox',class:'col-md-12',label:'Company?',value:true},
                    {field:'individual',type:'checkbox',class:'col-md-12',label:'Individual?',value:true},
                    {field:'other',type:'checkbox',class:'col-md-12',label:'Other?',value:false},
                   
                ],
                [
                    {   
                        field:'target_clients',
                        type:'select',class:'col-md-6',
                        label:'Target Clients',
                        options:[
                            {label:'Client type 1',value:'1'},
                            {label:'Client type 1',value:'2'},
                            {label:'Client type 1',value:'3'},
                        ],
                    value:'',required:true},
                    
                ],
                [{field:'annuel_turnover',type:'number',class:'col-md-6',label:'Annuel Turnover',value:'',required:false},],
                [{field:'Prospect_activity',type:'textarea',class:'col-md-6',label:'Prospect Activity',value:'',required:false},]
               
            ]
        },


        {
            label:"Current Business Information",
            blocs:[
                
                [
                    {
                        field: 'company_name',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Company Name',
                        value: '',
                        required: true
                    },
                   
                    {
                    field: 'address',
                    type: 'text',
                    class: 'col-md-6',
                    label: 'Address',
                    value: '',
                    required: true
                    } ,
                ],
                [
                    {
                        field: 'country',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Country',
                        value: '',
                        required: true
                        }
                        ,
                    {
                        field: 'city',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'City',
                        value: '',
                        required: true
                        }
                        ,
                ],
                [
                     
                    {
                        field: 'phone',
                        type: 'tel',
                        class: 'col-md-6',
                        label: 'Phone',
                        value: '',
                        required: true
                        },

                        
                    {
                        field: 'email',
                        type: 'email',
                        class: 'col-md-6',
                        label: 'Email',
                        value: '',
                        required: true
                        },
                  
                ],
                [
                    
                    {
                        field: 'year_of_establishment',
                        type: 'number',
                        class: 'col-md-6',
                        label: 'Year of establishment',
                        value: '',
                        required: true
                    },
                    {
                        field: 'signage',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Signage',
                        value: '',
                        required: true
                    },
                  
                ],
                [
                    {
                        field: 'activity',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Activity',
                        value: '',
                        required: true
                        },
                        {
                        field: 'legal_form',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Legal form',
                        value: '',
                        required: true
                        },
                ],
                [
                    {
                        field: 'company_registration_number',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Company Registration Number',
                        value: '',
                        required: true
                        }
                        ,
                        {
                        field: 'trade_register_number',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'N RCCM',
                        value: '',
                        required: true
                        }
                ],

               
                
               
            ]
          },
          {
            label:"Legal Representative Information",
            blocs:[
                [
                    {
                        field: 'legal_representative_name',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Name(s) and Surname(s)',
                        value: '',
                        required: true
                        }
                        ,
                        {
                        field: 'legal_representative_address',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Address',
                        value: '',
                        required: true
                        }
                        
                        ,
                ],
                [
                    {
                        field: 'legal_representative_phone',
                        type: 'tel',
                        class: 'col-md-6',
                        label: 'Phone',
                        value: '',
                        required: true
                        }
                        ,
                        {
                        field: 'legal_representative_email',
                        type: 'email',
                        class: 'col-md-6',
                        label: 'Email',
                        value: '',
                        required: true
                        }  
                ],
                [
                    {
                        field: 'identification_document',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Identification Document',
                        value: '',
                        required: true
                        }
                        ,
                        {
                        field: 'id_number',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'ID Number',
                        value: '',
                        required: true
                        },
                ],
              
               
            ]
          },
          {
            label:"Commercial Rental History",
            blocs:[
                [
                    {
                        field: 'previous_address',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Previous Address',
                        value: '',
                        required: false
                        }
                        ,
                        {
                        field: 'rental_history_city',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'City',
                        value: '',
                        required: false
                        }
                ],
                [
                    {
                        field: 'monthly_rent',
                        type: 'number',
                        class: 'col-md-6',
                        label: 'Monthly Rent',
                        value: '',
                        required: false
                        }


                  
                        
                ],

                [
                    {field:'lordland',type:'radio',class:'col-md-12',label:'lordland',value:true},
                    {field:'Locataire',type:'radio',class:'col-md-12',label:'Locataire',value:false},
                    {field:'no_applicable',type:'radio',class:'col-md-12',label:'No Applicable',value:false},
                   
                ],
                [
                    {
                        field: 'departure_reasons',
                        type: 'textarea',
                        class: 'col-md-6',
                        label: 'Reasons for Departure, (if applicable)',
                        value: '',
                        required: false
                    }
                ],
                [
                    {
                        field: 'landlord_name',
                        type: 'text',
                        class: 'col-md-6',
                        label: "Landlord's Name",
                        value: '',
                        required: false
                        }
                        ,
                        {
                        field: 'rental_history_phone',
                        type: 'tel',
                        class: 'col-md-6',
                        label: 'Phone',
                        value: '',
                        required: false
                        }
                ],
                [
                    {
                        field: 'rental_history_country',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Country',
                        value: '',
                        required: false
                        }
                        ,
                        {
                        field: 'lease_duration',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Lease Duration',
                        value: '',
                        required: false
                        }
                        
                      ,
                        {
                        field: 'rental_history_legal_representative',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'Legal Representative',
                        value: '',
                        required: false
                        }
                ]
            ]
          },
          {
            label:"Bank References",
            blocs:[
                [{
                    field: 'bank_name',
                    type: 'email',
                    class: 'col-md-6',
                    label: 'Bank Name:',
                    value: '',
                    required: true
                    }],
               [{
                field: 'bank_contact',
                type: 'text',
                class: 'col-md-6',
                label: 'Contact',
                value: '',
                required: true
                }
                
               ,
                {
                field: 'bank_phone',
                type: 'tel',
                class: 'col-md-6',
                label: 'Phone',
                value: '',
                required: true
                }],
               [{
                field: 'bank_email',
                type: 'email',
                class: 'col-md-6',
                label: 'Email',
                value: '',
                required: true
                }],
               
               
            ]
          },
          {
            label:"Description of the need:",
            blocs:[
                [{
                    field: 'interet_das',
                    type: 'select',
                    class: 'col-md-6',
                    label: 'Interest for which DAS?',
                    value: '',
                    options:[
                        {label:'Short-term rentals',value:'1'},
                        {label:'advertising space rental',value:'2'},
                        {label:'Leasing',value:'3'},
                        {label:'Technical Services',value:'4'},
                      ],
                    required: true
                    },

                    
                    {
                        field: 'surface_louable',
                        type: 'number',
                        class: 'col-md-6',
                        label: 'Surface louable souhaitée',
                        value: '',
                        required: true
                        }
                ],
                [{
                    field: 'electricity_supply',
                    type: 'select',
                    class: 'col-md-6',
                    label: 'Electricity supply:',
                    options:[
                        {label:'mandatory',value:'1'},
                        {label:'Not mandatory',value:'2'},
                    ],
                    value: false,
                    required: false
                    },
                
                    {
                        field: 'sprinkler',
                        type: 'select',
                        class: 'col-md-12',
                        label: 'Sprinkler:',
                        options:[
                            {label:'Mandatory',value:'1'},
                            {label:'Not Mandatory',value:'2'},
                        ],
                        
                        value: false,
                        required: false
                    }
                ],  
                
                [

                    {field:'electricity_supply: ',type:'checkbox',class:'col-md-12',label:'Electricity supply',value:false},
                    {field:'water_drainage',type:'checkbox',class:'col-md-12',label:'Water drainage',value:false},
                    {field:'external_storage',type:'checkbox',class:'col-md-12',label:'External storage',value:false},
                    
            
                ],
                [

                    {field:'mezzanine',type:'checkbox',class:'col-md-12',label:'Mezzanine',value:false},
                    {field:'marking',type:'checkbox',class:'col-md-12',label:'Parking',value:false},
                    {field:'sanitary',type:'checkbox',class:'col-md-12',label:'Sanitary',value:false},
                   
            
                ]
            ]
          },
      
        {
            label:"Current Business Information",
            blocs:[
                
                [
                    {field:'company_name',type:'text',class:'col-md-6',label:'Company Name',value:'',required:true},
                    
                    {field: 'address',type: 'text',class: 'col-md-6',label: 'Address',value: '', required: true},
                    {
                        field: 'city',
                        type: 'text',
                        class: 'col-md-6',
                        label: 'City',
                        value: '',
                        required: true
                        },
                    {field:'signage',type:'text',class:'col-md-6',label:'Signage',value:'',required:true},
                    {field:'Prospect_activity',type:'text',class:'col-md-6',label:'Prospect Activity',value:'',required:true},
                    {field:'Prospect_activity',type:'text',class:'col-md-6',label:'Prospect Activity',value:'',required:true},
                    
                    
                    
                ],
                [
                    {field:'telephone',type:'text',class:'col-md-3',label:'Telephone',value:'',required:true},
                    {field:'email',type:'text',class:'col-md-3',label:'Email',value:'',required:true},

                    
                ],
                [

                    {field:'rccm',type:'text',class:'col-md-3',label:'RCCM',value:'',required:true},
                    {field:'tax_regime',type:'text',class:'col-md-3',label:'Tax Regime',value:'',required:true},
                    
                ],
                [
                    {field:'website',type:'text',class:'col-md-6',label:'WEBSITE',value:''},
                    {field:'call_center',type:'text',class:'col-md-6',label:'Call Center',value:0},
                    {field:'vat_rate',type:'text',class:'col-md-6',label:'Vat Rate',value:0},
                ]
            
            ]
        },
        // {
        //     label:"Partner Contact",
        //     blocs:[
        //         [
        //             {field:'company',type:'checkbox',class:'col-md-12',label:'Company?',value:true},
        //             {field:'individual',type:'checkbox',class:'col-md-12',label:'Individual?',value:true},
        //             {field:'other',type:'checkbox',class:'col-md-12',label:'Other?',value:false},
                   
        //         ],
        //         [
        //             {field:'company_name',type:'text',class:'col-md-6',label:'Company Name',value:'',required:true},
        //             {field:'signage',type:'text',class:'col-md-6',label:'Signage',value:'',required:true},
        //             {field:'Prospect_activity',type:'text',class:'col-md-6',label:'Prospect Activity',value:'',required:true},
        //             {field:'Prospect_activity',type:'text',class:'col-md-6',label:'Prospect Activity',value:'',required:true},
                   
        //         ],
        //         [
        //             {field:'telephone',type:'text',class:'col-md-3',label:'Telephone',value:'',required:true},
        //             {field:'email',type:'text',class:'col-md-3',label:'Email',value:'',required:true},

                    
        //         ],
        //         [

        //             {field:'rccm',type:'text',class:'col-md-3',label:'RCCM',value:'',required:true},
        //             {field:'tax_regime',type:'text',class:'col-md-3',label:'Tax Regime',value:'',required:true},
                    
        //         ],
        //         [
        //             {field:'website',type:'text',class:'col-md-6',label:'WEBSITE',value:''},
        //             {field:'call_center',type:'text',class:'col-md-6',label:'Call Center',value:0},
        //             {field:'vat_rate',type:'text',class:'col-md-6',label:'Vat Rate',value:0},
        //         ]
            
        //     ]
        // },
        {
            label:"Contacts",
            blocs:[
                [
                  {field:'title',type:'select',class:'col-md-12',label:'Title',value:'',options:[
                    {label:'General Manager',value:'1'},
                    {label:'Commercial',value:'2'},
                    {label:'Finance',value:'3'},
                    {label:'Technical Services',value:'4'},
                  ]},
                  {field:'poc_name',type:'text',class:'col-md-12',label:'Name',value:''},
                  {field:'poc_phone',type:'text',class:'col-md-12',label:'phone',value:''},
                  {field:'poc_email',type:'text',class:'col-md-12',label:'Email',value:''},
                   
                ],

                [
                  {field:'submit_poc',type:'button',class:'col-md-12',label:'AJOUTER',function:function() {
                    console.log("hello");
                  }},
                   
                ],
               
            ]
        },
        // {
        //     label:"Company Overview",
        //     blocs:[
        //         [
        //             {field:'gerneral_details_f_g_s',type:'text',class:'col-md-12',label:'Gerneral details of goods and services',value:''},
                   
        //         ],
        //         [
        //             {field:'date_company_established',type:'text',class:'col-md-12',label:'Date company Established',value:''},
        //             {field:'gross_annual_sales',type:'text',class:'col-md-12',label:'Gross Annual Sales',value:''},
        //         ],
        //         [
        //             {field:'geographique_service_area',type:'text',class:'col-md-12',label:'Georgraphique Service Area',value:''},
        //             {field:'legal_structure',type:'text',class:'col-md-12',label:'Legal structure',value:''},
        //         ],
        //         [
        //             {field:'business_type',type:'text',class:'col-md-12',label:'Business Type',value:''},
        //             {field:'years_previously_registreted',type:'text',class:'col-md-12',label:'Years previously Registreted',value:''},
        //         ],
        //         [
        //             {field:'business_type',type:'text',class:'col-md-12',label:'Business Type',value:''},
        //             {field:'years_previously_registreted',type:'text',class:'col-md-12',label:'Years previously Registreted',value:''},
        //         ],
        //         [
        //             {field:'insured',type:'checkbox',class:'col-md-12',label:'Insured?',value:true},
        //             {field:'bonded',type:'checkbox',class:'col-md-12',label:'Bonded?',value:false},
        //             {field:'licensed',type:'checkbox',class:'col-md-12',label:'Licensed?',value:false},
        //         ],
        //         [
        //             {field:'license_number',type:'text',class:'col-md-12',label:'License Number',value:''},
        //         ],
        //     ]
        // },
        // {
        //   label:"Products/Services",
        //   blocs:[
        //       [
        //         {field:'products_services',type:'select',class:'col-md-12',label:'Select Products or services',value:'',options:[
        //           {label:'Product One',value:'1'},
        //           {label:'Product Two',value:'2'},
        //           {label:'Product Three',value:'3'},
        //         ]},

        //         {field:'submit_p_s',type:'button',class:'col-md-12',label:'SELECT',function:function() {
        //           console.log("hello");
        //         }},
        //       ],
             
        //   ]
        // },
        
        // {
        //   label:"Products/Services",
        //   blocs:[
        //       [
        //         {field:'products_services',type:'select',class:'col-md-12',label:'Select Products or services',value:'',options:[
        //           {label:'Product One',value:'1'},
        //           {label:'Product Two',value:'2'},
        //           {label:'Product Three',value:'3'},
        //         ]},

        //         {field:'submit_p_s',type:'button',class:'col-md-12',label:'SELECT',function:function() {
        //           console.log("hello");
        //         }},
        //       ],
             
        //   ]
        // },
        // {
        //     label:"fin",
        //     blocs:[
        //         [],
        //         [],
        //         [],
        //         [],
                  
               
        //     ]
        //   },
        
    ];
    const fields =[]

    steps.forEach(step => {
        step.blocs.forEach( bloc =>{
            bloc.forEach(field=>{
                fields.push(field)
                
            })
        });  
    });

    const sectionStyle= {
        width: `calc(75vw - 100px)`,
       
      };

    function handleSubmit(event) {
        event.preventDefault();
        const formData = new FormData();

            fields.map((item,index)=>{
                formData.append(item.field,fieldStates[index].value );
            })

            console.log(formData);
            const reqAddProduct = {
                method: 'POST',
                body:formData
            }
    
            // // console.log(reqOrderItem)
            // fetch('http://127.0.0.1:9000/api/products/', reqAddProduct)
            // .then(response => response.json())
            // .then(data => setProductAdded(true));
      }

    const [fieldStates, setFieldStates] = useState(fields);
    const [activeStep, setActiveStep] = useState(0);
    let countField = 0;
  return (
        <div id='stepper-form '>
        <div   className="mx-auto container stepper-vertical">

        {/* <!-- Progress Form --> */}
        <form id="progress-form" className="p-4 progress-form" action="https://httpbin.org/post" lang="en" novalidate>
    
        {/* <!-- Step Navigation --> */}
        <div className="d-flex align-items-start mb-3 sm:mb-5 progress-form__tabs" role="tablist">
            {
                steps.map((item,index)=>(
                    <button key={index} id="progress-form__tab-1" className="flex-1 px-0 pt-2 progress-form__tabs-item" type="button" role="tab" 
                    aria-controls="progress-form__panel-1"
                    data-complete={index<activeStep?"true":"false"}
                    aria-selected={index===activeStep?"true":"false"} onClick={()=>{setActiveStep(index)}}  >
                        {/* <span className="d-block step" aria-hidden="true">Step {index+1} <span className="sm:d-none">of {steps.length}</span></span> */}
                        {item.label}
                    </button>
                ))
            }
        </div>
    
    
    
    
    {
    steps.map((step, index) => (
            <section style={sectionStyle} id={`progress-form__panel-${index}`} role="tabpanel" aria-labelledby={`progress-form__tab-${index}`} tabIndex="0" hidden={index === activeStep ? false : true}>
            <div className='row sm:d-grid sm:grid-col-1 sm:mt-3'>
                <h3> {step.label}</h3>
            </div>

            {step.blocs.map((bloc, indexBloc) => (
                <div key={`bloc${indexBloc}`} className={"row sm:d-grid sm:grid-col-"+bloc.length+" sm:mt-3"}>
                {bloc.map((field, indexField) => {
                    const currentCount = countField++; // Increment the counter and store the current value
                    switch (fieldStates[currentCount].type) {
                    case 'select':
    
                    return <div key={currentCount} className="mt-3 sm:mt-0 form__field">
                        <label htmlFor="">
                        {fieldStates[currentCount].label}
                        <span data-required={fieldStates[currentCount].required?"true":"false"} aria-hidden="true"></span>
                        </label>
                        <select className="form-control"  value={fieldStates[currentCount].value} onChange={(event) => {
                            const updatedField = [...fieldStates];
                            updatedField[currentCount].value = event.target.value;
                            setFieldStates(updatedField);
                        }}>
                        {
                            fieldStates[currentCount].options.map((option)=>{
                                return <option key={option.label} value={option.value} >{option.label}</option>
                            })
                        }
                    </select>
                    </div>
    
    
                        break;
                        case 'button':
    
                        return <div key={currentCount} className="mt-3 sm:mt-0 form__field">
    
                        <button onClick={fieldStates[currentCount].function}>{fieldStates[currentCount].label}</button>
                        
                    </div>
    
    
                        break;
                        case 'checkbox':
    
                        return <div key={currentCount}  class="mt-1 form__field">
                    <label class="form__choice-wrapper">
                        <input  type="checkbox" checked={fieldStates[currentCount].value}
                                onChange={(event) => {
                                const updatedField = [...fieldStates];
                                updatedField[currentCount].value = event.target.checked;
                                setFieldStates(updatedField);
                                }} />
                        <span>{fieldStates[currentCount].label}</span>
                    </label>
                    </div>
                        case 'textarea':
    
                        return <div key={currentCount} className={fieldStates[currentCount].class+" mt-3 sm:mt-0 form__field"}>
                        <label htmlFor="">
                        {fieldStates[currentCount].label}
                        <span data-required={fieldStates[currentCount].required?"true":"false"} aria-hidden="true"></span>
                        </label>
                        
                        <textarea
                        className="form-control"
                        type={fieldStates[currentCount].type}
                        onChange={(event) => {
                            const updatedField = [...fieldStates];
                            updatedField[currentCount].value = event.target.value;
                            setFieldStates(updatedField);
                        }}
                        required={(fieldStates[currentCount].required)?true:false}
                        value={fieldStates[currentCount].value}
                        ></textarea>
                    </div>
    
    
                        break;
                    default:
                        return <div key={currentCount} className={fieldStates[currentCount].class+" mt-3 sm:mt-0 form__field"}>
                        <label htmlFor="">
                        {fieldStates[currentCount].label}
                        <span data-required={fieldStates[currentCount].required?"true":"false"} aria-hidden="true"></span>
                        </label>
                        
                        <input
                        className="form-control"
                        type={fieldStates[currentCount].type}
                        onChange={(event) => {
                            const updatedField = [...fieldStates];
                            updatedField[currentCount].value = event.target.value;
                            setFieldStates(updatedField);
                        }}
                        required={(fieldStates[currentCount].required)?true:false}
                        value={fieldStates[currentCount].value}
                        />
                    </div>
                    break;
                    }
                })}
                </div>
            ))}
            <div className="d-flex flex-column-reverse sm:flex-row align-items-center justify-center sm:justify-end mt-4 sm:mt-5">
            <button type="button" style={{display: (index===0)?"none":"block"}} className="mt-1 sm:mt-0 button--simple" data-action="prev" onClick={()=>{setActiveStep(index-1)}}>
                Back
            </button>
            <button type="button" style={{display: (index===steps.length-1)?"none":"block"}} data-action="next" onClick={()=>{setActiveStep(index+1)}}>
                Continue
            </button>
            <button type="button" style={{display: (index<steps.length-1)?"none":"block"}} data-action="next" onClick={handleSubmit}>
                submit
            </button>
            </div>
            </section>
        ))}
    
        {/* <!-- / End Step 2 --> */}
        </form>
        {/* <!-- / End Progress Form --> */}
    
        </div>
        </div>
  
  );
};

export default StepperFormVertical;