

import React, { useState ,useContext } from 'react';
import './stepper_form.css';

const StepperForm = (props) => {

    const steps = [
        {
            label:"Partner Contact",
            blocs:[
              [
                {field:'company',type:'checkbox',class:'col-md-12',label:'Company?',value:true},
                {field:'individual',type:'checkbox',class:'col-md-12',label:'Individual?',value:true},
                {field:'other',type:'checkbox',class:'col-md-12',label:'Other?',value:false},
               
            ],
                [
                    {field:'company_name',type:'text',class:'col-md-2',label:'Company Name',value:'',required:true},
                   
                ],
                [
                    {field:'telephone',type:'text',class:'col-md-3',label:'Telephone',value:'',required:true},
                    {field:'email',type:'text',class:'col-md-3',label:'Email',value:'',required:true},

                    
                ],
                [

                    {field:'rccm',type:'text',class:'col-md-3',label:'RCCM',value:'',required:true},
                    {field:'tax_regime',type:'text',class:'col-md-3',label:'Tax Regime',value:'',required:true},
                    
                ],
                [
                    {field:'website',type:'date',class:'col-md-6',label:'WEBSITE',value:''},
                    {field:'call_center',type:'text',class:'col-md-6',label:'Call Center',value:0},
                    {field:'vat_rate',type:'text',class:'col-md-6',label:'Vat Rate',value:0},
                ]
            
            ]
        },
        {
            label:"Point of contact",
            blocs:[
                [
                  {field:'title',type:'select',class:'col-md-12',label:'Title',value:'',options:[
                    {label:'General Manager',value:'1'},
                    {label:'Commercial',value:'2'},
                    {label:'Finance',value:'3'},
                    {label:'Technical Services',value:'4'},
                  ]},
                  {field:'poc_name',type:'text',class:'col-md-12',label:'Name',value:''},
                  {field:'poc_phone',type:'text',class:'col-md-12',label:'phone',value:''},
                  {field:'poc_email',type:'text',class:'col-md-12',label:'Email',value:''},
                   
                ],

                [
                  {field:'submit_poc',type:'button',class:'col-md-12',label:'AJOUTER',function:function() {
                    console.log("hello");
                  }},
                   
                ],
               
            ]
        },
        {
            label:"Company Overview",
            blocs:[
                [
                    {field:'gerneral_details_f_g_s',type:'text',class:'col-md-12',label:'Gerneral details of goods and services',value:''},
                   
                ],
                [
                    {field:'date_company_established',type:'text',class:'col-md-12',label:'Date company Established',value:''},
                    {field:'gross_annual_sales',type:'text',class:'col-md-12',label:'Gross Annual Sales',value:''},
                ],
                [
                    {field:'geographique_service_area',type:'text',class:'col-md-12',label:'Georgraphique Service Area',value:''},
                    {field:'legal_structure',type:'text',class:'col-md-12',label:'Legal structure',value:''},
                ],
                [
                    {field:'business_type',type:'text',class:'col-md-12',label:'Business Type',value:''},
                    {field:'years_previously_registreted',type:'text',class:'col-md-12',label:'Years previously Registreted',value:''},
                ],
                [
                    {field:'business_type',type:'text',class:'col-md-12',label:'Business Type',value:''},
                    {field:'years_previously_registreted',type:'text',class:'col-md-12',label:'Years previously Registreted',value:''},
                ],
                [
                    {field:'insured',type:'checkbox',class:'col-md-12',label:'Insured?',value:true},
                    {field:'bonded',type:'checkbox',class:'col-md-12',label:'Bonded?',value:false},
                    {field:'licensed',type:'checkbox',class:'col-md-12',label:'Licensed?',value:false},
                ],
                [
                    {field:'license_number',type:'text',class:'col-md-12',label:'License Number',value:''},
                ],
            ]
        },
        {
          label:"Products/Services",
          blocs:[
              [
                {field:'products_services',type:'select',class:'col-md-12',label:'Select Products or services',value:'',options:[
                  {label:'Product One',value:'1'},
                  {label:'Product Two',value:'2'},
                  {label:'Product Three',value:'3'},
                ]},

                {field:'submit_p_s',type:'button',class:'col-md-12',label:'SELECT',function:function() {
                  console.log("hello");
                }},
              ],
             
          ]
      }
    ];
    const fields =[]

    steps.forEach(step => {
        step.blocs.forEach( bloc =>{
            bloc.forEach(field=>{
                fields.push(field)
                
            })
        });  
    });

    function handleSubmit(event) {
        event.preventDefault();
        const formData = new FormData();

            fields.map((item,index)=>{
                formData.append(item.field,fieldStates[index].value );
            })

            console.log(formData);
            const reqAddProduct = {
                method: 'POST',
                body:formData
            }
    
            // // console.log(reqOrderItem)
            // fetch('http://127.0.0.1:9000/api/products/', reqAddProduct)
            // .then(response => response.json())
            // .then(data => setProductAdded(true));
      }

    const [fieldStates, setFieldStates] = useState(fields);
    const [activeStep, setActiveStep] = useState(0);
    let countField = 0;
  return (
    <div className="mx-auto container ">

  {/* <!-- Progress Form --> */}
  <form id="progress-form" className="p-4 progress-form" action="https://httpbin.org/post" lang="en" novalidate>

    {/* <!-- Step Navigation --> */}
    <div className="d-flex align-items-start mb-3 sm:mb-5 progress-form__tabs" role="tablist">
        {
            steps.map((item,index)=>(
                <button key={index} id="progress-form__tab-1" className="flex-1 px-0 pt-2 progress-form__tabs-item" type="button" role="tab" 
                aria-controls="progress-form__panel-1"
                data-complete={index<activeStep?"true":"false"}
                 aria-selected={index===activeStep?"true":"false"}  >
                    <span className="d-block step" aria-hidden="true">Step {index+1} <span className="sm:d-none">of {steps.length}</span></span>
                    {item.label}
                </button>
            ))
        }
    </div>




{steps.map((step, index) => (
      <section id={`progress-form__panel-${index}`} role="tabpanel" aria-labelledby={`progress-form__tab-${index}`} tabIndex="0" hidden={index === activeStep ? false : true}>
        {step.blocs.map((bloc, indexBloc) => (
          <div key={`bloc${indexBloc}`} className={"row sm:d-grid sm:grid-col-"+bloc.length+" sm:mt-3"}>
            {bloc.map((field, indexField) => {
              const currentCount = countField++; // Increment the counter and store the current value
              switch (fieldStates[currentCount].type) {
                case 'select':

                 return <div key={currentCount} className="mt-3 sm:mt-0 form__field">
                  <label htmlFor="first-name">
                    {fieldStates[currentCount].label}
                    <span data-required={fieldStates[currentCount].required?"true":"false"} aria-hidden="true"></span>
                  </label>
                  <select className="form-control"  value={fieldStates[currentCount].value} onChange={(event) => {
                      const updatedField = [...fieldStates];
                      updatedField[currentCount].value = event.target.value;
                      setFieldStates(updatedField);
                    }}>
                    {
                        fieldStates[currentCount].options.map((option)=>{
                            return <option key={option.label} value={option.value} >{option.label}</option>
                        })
                    }
                </select>
                </div>


                  break;
                  case 'button':

                  return <div key={currentCount} className="mt-3 sm:mt-0 form__field">

                    <button onClick={fieldStates[currentCount].function}>{fieldStates[currentCount].label}</button>
                   
                 </div>
 
 
                   break;
                  case 'checkbox':

                  return <div key={currentCount}  class="mt-1 form__field">
                 <label class="form__choice-wrapper">
                   <input  type="checkbox" checked={fieldStates[currentCount].value}
                           onChange={(event) => {
                            const updatedField = [...fieldStates];
                            updatedField[currentCount].value = event.target.checked;
                            setFieldStates(updatedField);
                          }} />
                   <span>{fieldStates[currentCount].label}</span>
                 </label>
               </div>
 
 
                   break;
                default:
                  return <div key={currentCount} className={fieldStates[currentCount].class+" mt-3 sm:mt-0 form__field"}>
                  <label htmlFor="first-name">
                    {fieldStates[currentCount].label}
                    <span data-required={fieldStates[currentCount].required?"true":"false"} aria-hidden="true"></span>
                  </label>
                  
                  <input
                    className="form-control"
                    type={fieldStates[currentCount].type}
                    onChange={(event) => {
                      const updatedField = [...fieldStates];
                      updatedField[currentCount].value = event.target.value;
                      setFieldStates(updatedField);
                    }}
                    required={(fieldStates[currentCount].required)?true:false}
                    value={fieldStates[currentCount].value}
                  />
                </div>
                break;
              }
            })}
          </div>
        ))}
        <div className="d-flex flex-column-reverse sm:flex-row align-items-center justify-center sm:justify-end mt-4 sm:mt-5">
        <button type="button" style={{display: (index===0)?"none":"block"}} className="mt-1 sm:mt-0 button--simple" data-action="prev" onClick={()=>{setActiveStep(index-1)}}>
          Back
        </button>
        <button type="button" style={{display: (index===steps.length-1)?"none":"block"}} data-action="next" onClick={()=>{setActiveStep(index+1)}}>
          Continue
        </button>
        <button type="button" style={{display: (index<steps.length-1)?"none":"block"}} data-action="next" onClick={handleSubmit}>
          submit
        </button>
      </div>
      </section>
    ))}

    {/* <!-- / End Step 2 --> */}
  </form>
  {/* <!-- / End Progress Form --> */}

</div>

  );
};

export default StepperForm;